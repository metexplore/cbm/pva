import miom
import numpy as np
from pva.utils import network2model, imat, load_reaction_weights
from pva.pathway_variability import pathway_variability_analysis
from pva.fisher_pathways import fisher_pathways


if __name__ == "__main__":

    condition = "PFOAhigh"

    modelpath = "@SysBioChalmers/Zebrafish-GEM/v1.1.0/default.miom"

    cobra_weights = load_reaction_weights("zebrafish_weights_%s.csv" % condition)

    network = miom.load_gem(modelpath)

    weights = np.zeros(len(cobra_weights))
    for i in range(len(network.R)):
        weights[i] = cobra_weights[network.R['id'][i]]

    eps = 1e-3
    thr = 1e-5
    obj_tol = 1e-3
    t = 60000  # timelimit for solver

    model = network2model(network)
    imatsol = imat(model, weights, eps=eps, thr=thr, save=True)

    pathways = []
    [pathways.append(x) for x in model.network.R["subsystem"] if x not in pathways]

    removed_pathways = ["Transport reactions", "Miscellaneous", "Isolated", "Exchange/demand reactions",
                        "Pool reactions", "Transport"]

    pathway_list = list(set(pathways)-set(removed_pathways))
    pathway_list.sort()
    pathway_list = pathway_list[:5]  # only use the first 5 pathways for a short test

    activities, times = pathway_variability_analysis(modelpath=modelpath, weights=weights, eps=eps, thr=thr,
                                                     prev_sol=imatsol, pathway_list=pathway_list, obj_tol=obj_tol,
                                                     tlim=t)
    activities.to_csv("zebrafish_%s_activities.csv" % condition)
    times.to_csv("zebrafish_%s_times.csv" % condition)

    fluxconsistent_model = network2model(miom.load_gem(modelpath)).subset_selection(1).solve().select_subnetwork()

    # calculate the -log10(pval) of over/underrepresentation of active reactions in the pathways
    over, under = fisher_pathways(fluxconsistent_model, imatsol, "zebrafish_%s_activities.csv" % condition,
                                  out_path=condition+"_pathways")
