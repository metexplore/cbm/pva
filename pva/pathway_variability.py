import miom
import picos
import time
import numpy as np
import pandas as pd
from pva.utils import network2model, make_cobra_solution, write_solution


def pathway_variability_analysis(modelpath, weights, eps=1e-3, thr=1e-5, prev_sol=None, pathway_list=None, obj_tol=1e-3,
                                 tlim=None, out_path=''):
    """
    This function computes, for a given subset-selection problem, the maximum and minimum number of possible
    active reactions for each pathway

    :param modelpath: a path to a miom model
    :param weights: a numpy array of reaction weights for the subset-selection
    :param eps:  the minimum flux value of reactions with positive weight
    :param thr: the minimum flux value for a reaction to be considered active
    :param prev_sol: a previous subset-selection solution (if None, a solution will be computed)
    :param pathway_list: a list of pathways to be analyzed (if None, all pathways present in the model will be used)
    :param obj_tol: relative tolerance in the optimality of the objective value from the previous solution
    :param tlim: timelimit for the solver optimization call
    :param out_path: path to which the solutions are saved
    :return all_activities: a pandas DataFrame containing the minimal and maximal number of active reactions for
    each pathway, as well as the number of active reactions in the original subset-selection solution
    """
    model = network2model(miom.load_gem(modelpath), timelimit=tlim)
    times = pd.Series()
    all_activities = pd.DataFrame(columns=['min', 'imat', 'max'])

    if not prev_sol:
        model.subset_selection(weights, eps=eps).solve()
        prev_sol = make_cobra_solution(model)

    optimal_value = prev_sol.objective_value-prev_sol.objective_value*obj_tol

    if not pathway_list:
        pathway_list = []
        [pathway_list.append(x) for x in model.network.R['subsystem'] if x not in pathway_list]

    for path_name in pathway_list:
        rxns = model.network.find_reactions_from_pathway(path_name)
        pathway_rxns = np.where(rxns == 1)[0].tolist()

        pathway_imat_activity = (np.abs(prev_sol.fluxes[pathway_rxns]) >= thr).astype(int)

        rh_pos_data = []
        rh_neg_data = []
        rl_data = []
        lenpos = 0
        lenneg = 0

        for idx in pathway_rxns:
            r = model.network.R[idx]
            rid, lb, ub = r['id'], r['lb'], r['ub']
            if ub > 0:
                rh_pos_data.append((lenpos, idx, lb, ub))
                lenpos += 1
            if lb < 0:
                rh_neg_data.append((lenpos, idx, lb, ub))
                lenpos += 1
            if abs(lb) + abs(ub) > 0:
                rl_data.append((lenneg, idx, lb, ub))
                lenneg += 1
        rh_pos_data = [np.array(x) for x in list(zip(*rh_pos_data))]
        rh_neg_data = [np.array(x) for x in list(zip(*rh_neg_data))]
        rl_data = [np.array(x) for x in list(zip(*rl_data))]

        # max side: maximizing the number of active reactions in the pathway
        t0 = time.perf_counter()

        model_temp = network2model(miom.load_gem(modelpath), timelimit=tlim).subset_selection(weights, eps=eps)
        model_temp.add_constraint(sum(model_temp.variables.indicators) >= (optimal_value))

        V = model_temp.variables.fluxvars
        P = model_temp.problem
        Y = picos.BinaryVariable('Y', shape=(lenpos,))
        C = picos.Constant('C', value=np.ones(lenpos))

        if rh_pos_data:
            I, J, LB, UB = rh_pos_data
            EA = eps - LB
            EB = Y[I] ^ EA
            expr1 = V[J] >= LB + EB
            P.add_constraint(expr1)

        if rh_neg_data:
            I, J, LB, UB = rh_neg_data
            EA = eps + UB
            EB = Y[I] ^ EA
            expr2 = V[J] <= UB - EB
            P.add_constraint(expr2)

        P.set_objective('max', C.T * Y)
        model_temp.variables.pathway_indicators = Y

        model_temp.problem.options.primals = None
        model_temp.solve()

        pathway_max_activity = (np.abs(model_temp.variables.flux_values[pathway_rxns]) >= thr).astype(int)
        maxsol = make_cobra_solution(model_temp)
        write_solution(model_temp, maxsol, thr, out_path+path_name.replace('/', '_')+' maxsol.csv')

        t1 = time.perf_counter()
        times[path_name+' max'] = t1-t0
        print('time for %s maximum:' % path_name, t1-t0)

        # min side: maximizing the number of inactive reactions in the pathway
        t0 = time.perf_counter()

        model_temp = network2model(miom.load_gem(modelpath), timelimit=tlim).subset_selection(weights, eps=eps)
        model_temp.add_constraint(sum(model_temp.variables.indicators) >= (optimal_value))

        V = model_temp.variables.fluxvars
        P = model_temp.problem

        Y = picos.BinaryVariable('Y', shape=(lenneg,))
        C = picos.Constant('C', value=np.ones(lenneg))

        if rl_data:
            I, J, LB, UB = rl_data
            EA = 1 - Y[I]
            EB = EA ^ UB
            EC = EA ^ LB
            expr1 = EB - V[J] >= 0
            expr2 = EC - V[J] <= 0
            P.add_constraint(expr1)
            P.add_constraint(expr2)

        P.set_objective('max', C.T * Y)
        model_temp.variables.pathway_indicators = Y

        model_temp.problem.options.primals = None
        model_temp.solve()

        pathway_min_activity = (np.abs(model_temp.variables.flux_values[pathway_rxns]) >= thr).astype(int)
        minsol = make_cobra_solution(model_temp)
        write_solution(model_temp, minsol, thr, out_path+path_name.replace('/', '_')+' minsol.csv')

        t1 = time.perf_counter()
        times[path_name+' min'] = t1-t0
        activities = [sum(pathway_min_activity), sum(pathway_imat_activity), sum(pathway_max_activity)]
        all_activities.loc[path_name] = activities
        print('time for %s minimum:' % path_name, t1-t0)
        print(activities)

    return all_activities, times
