import miom
import cobra
import picos
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from cobra import Solution
from miom.miom import _RxnVar, _ReactionType


def network2model(network, solver='cplex', opt_tol=None, timelimit=None):
    model = miom.load(network, solver=solver)
    if not model.variables.fluxvars:
        model.steady_state()
    if timelimit:
        if type(model) == miom.miom.PicosModel:
            model.problem.options.timelimit = timelimit
        elif type(model) == miom.miom.PythonMipModel:
            model.problem.max_seconds = timelimit
        else:
            raise TypeError('unrecgonized model type %s, expected either PicosModel or PythonMipModel ', type(model))
    if opt_tol:
        model.setup(opt_tol=opt_tol)

    return model


def make_cobra_solution(model):
    fluxes = pd.Series(model.variables.flux_values, index=model.network.R['id'], name='fluxes')
    solution = cobra.Solution(model.status['objective_value'], model.status['status'], fluxes)
    return solution


def get_miom_binary_sol(model, threshold):
    binary = list((np.abs(model.variables.flux_values) >= threshold).astype(int))
    return binary


def get_cobra_binary_sol(solution, threshold, tolerance):
    binary = [1 if np.abs(flux) >= (threshold-tolerance) else 0 for flux in solution.fluxes]
    return binary


def save_reaction_weights(reaction_weights, filename):
    """
    Parameters
    ----------
    reaction_weights: dict
        a dictionary where keys = reaction IDs and values = weights
    filename: str
    Returns
    -------
    the reaction_weights dict as a pandas DataFrame
    """
    df = pd.DataFrame(reaction_weights.items(), columns=['reactions', 'weights'])
    df.to_csv(filename)
    df.index = df['reactions']
    return df['weights']


def load_reaction_weights(filename, rxn_names='reactions', weight_names='weights'):
    """
    loads reaction weights from a .csv file
    Parameters
    ----------
    filename: str
        the path + name of a .csv file containing reaction weights
    rxn_names: str
        the name of the column containing the reaction names
    weight_names: str
        the name of the column containing the weights

    Returns
    -------
    a dict of reaction weights
    """
    df = pd.read_csv(filename)
    df.index = df[rxn_names]
    reaction_weights = df[weight_names].to_dict()
    return {k: float(v) for k, v in reaction_weights.items() if float(v) == float(v)}


def write_solution(model, solution, threshold, filename='imat_sol.csv'):
    """
    Writes an optimize solution as a txt file. The solution is written in a column format
    Parameters
    ----------
    solution: cobra.Solution
    threshold: float
    filename: str
    """
    tol = model.problem.options.rel_prim_fsb_tol
    solution_binary = get_cobra_binary_sol(solution, threshold, tol)

    with open(filename, 'w+') as file:
        file.write('reaction,fluxes,binary\n')
        for i, v in enumerate(solution.fluxes):
            file.write(solution.fluxes.index[i]+','+str(v)+','+str(solution_binary[i])+'\n')
        file.write('objective value: %f\n' % solution.objective_value)
        file.write('solver status: %s' % solution.status)


def read_solution(filename, model=None):
    binary = True
    with open(filename, 'r') as f:
        reader = f.read().split('\n')
        if reader[0] == 'reaction,fluxes,binary':
            binary = False
            if reader[-1] == '':
                reader.pop(-1)
            objective_value = float(reader[-2].split()[-1])
            status = reader[-1].split()[-1]
    if binary:
        fluxes = pd.read_csv(filename, index_col=0).rename(index={0: 'fluxes'}).T
        fluxes.index = model.network.R['id'].tolist()
        sol_bin = list(fluxes['fluxes'])
        objective_value = 0.
        status = 'binary'
    else:
        df = pd.read_csv(filename, index_col=0, skipfooter=2, engine='python')
        fluxes = df['fluxes']
        sol_bin = df['binary'].to_list()
    solution = Solution(objective_value, status, fluxes)
    return solution, sol_bin


def imat(model, reaction_weights, eps=1e-3, thr=1e-5, export_network=False, save=True, out_name='miom_imat_sol.csv'):

    model.subset_selection(reaction_weights, eps=eps).solve()
    imat_network = model.select_subnetwork(value=thr)

    solution = make_cobra_solution(model)

    if export_network:
        miom.mio.export_gem(imat_network, 'imat_network.miom')
    if save:
        write_solution(model, solution=solution, threshold=thr, filename=out_name)

    return solution


def setup_model_from_solution(model, solution, eps, thr):
    model.variables.fluxvars.__setattr__('value', solution.fluxes.values)
    ivs = np.zeros(len(model.variables.indicators))
    for idx, r in enumerate(model.variables.assigned_reactions):
        if r.type == _ReactionType.RH_POS and solution.fluxes[r.id] >= eps:
            ivs[idx] = 1
        elif r.type == _ReactionType.RH_NEG and solution.fluxes[r.id] <= -eps:
            ivs[idx] = 1
        elif r.type == _ReactionType.RL and abs(solution.fluxes[r.id]) < thr:
            ivs[idx] = 1
    model.variables.indicators.__setattr__('value', ivs)

    primals = {model.variables.fluxvars: solution.fluxes.values, model.variables.indicators: ivs}
    model.solutions = picos.Solution(primals=primals, problem=model.problem, solver='cplex', primalStatus='optimal',
                                     problemStatus='feasible', reportedValue=solution.objective_value)
    return True


def plot_activity_histograms(activities, pathway_rxns):
    activities[2] += 1
    sols = pd.read_csv('quantfilt1_removed_25_divenum_solutions_obj1e2.csv', index_col=0)
    rxns_str = [str(x) for x in pathway_rxns]
    data = sols[rxns_str].sum(axis=1)
    data.hist(bins=np.arange(min(data), max(data) + 2))
    plt.hist(activities, bins=activities[2]-activities[0], color='red')
    plt.xticks(np.arange(activities[0], activities[2]+1))
    plt.title('Activity of Cholesterol metabolism in DEXOM solutions')
    plt.xlabel('number of active reactions in pathway')
    plt.ylabel('number of solutions')
    return 0
