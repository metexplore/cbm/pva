import pandas as pd
import numpy as np
from scipy.stats import fisher_exact
from statsmodels.stats.multitest import fdrcorrection


def fisher_pathways(model, imatsol, act_path, out_path="fischer_pathways", thr=1e-6):

    imatbin = (np.abs(imatsol.fluxes) >= thr).astype(int)
    active_rxns = imatbin.sum()

    activities = pd.read_csv(act_path, index_col=0)

    pvalsu = {}
    pvalso = {}
    for x in activities.iterrows():
        tempu = []
        tempo = []
        for y in activities.columns:
            active_in_path = x[1][y]
            rxns_in_path = model.network.find_reactions_from_pathway(x[0]).sum()
            table = np.array([[active_in_path, rxns_in_path - active_in_path],
                              [active_rxns - active_in_path,
                               len(model.network.R) - rxns_in_path - active_rxns + active_in_path]])
            o, pu = fisher_exact(table, alternative='less')
            o, po = fisher_exact(table, alternative='greater')
            tempu.append(pu)
            tempo.append(po)
        pvalsu[x[0]] = tempu
        pvalso[x[0]] = tempo
    over = pd.DataFrame(pvalso)
    under = pd.DataFrame(pvalsu)

    t, fdr = fdrcorrection(over.values.flatten())
    over = pd.DataFrame(-np.log10(fdr).reshape(over.shape), columns=over.columns)
    over.index = ["min", "imat", "max"]
    over.T.to_csv(out_path + "_over.csv", sep=";")

    t, fdr = fdrcorrection(under.values.flatten())
    under = pd.DataFrame(-np.log10(fdr).reshape(under.shape), columns=under.columns)
    under.index = ["min", "imat", "max"]
    under.T.to_csv(out_path + "_under.csv", sep=";")

    return over, under
