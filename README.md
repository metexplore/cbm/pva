# pathway variability analysis

The aim of this project is to analyze the variability of metabolic pathways in context-specific metabolic networks.

The `main.py` script contains an example of pathway variability using the Zebrafish1 model.

First, an imat solution is computed in order to obtain a context-specific network.  
In the `pathway_variability`function, the number of active reactions in each pathway is maximized and minimized, while keeping the optimality of the imat solution.  
This allows us to obtain the minimal and maximal activity of each metabolic pathway within the constraints created by the subset-selection problem (the activity is defined as the number of active reactions per pathway).  
The activity of the original imat solution is given as a reference point.
